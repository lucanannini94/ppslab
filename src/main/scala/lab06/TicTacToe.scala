package lab06

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Double, y: Double, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Double, y: Double): Option[Player] = board.collectFirst{case Mark(`x`, `y`, p) => p}

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    for( x <- 0 until 3;
         y <- 0 until 3;
         if find(board, x, y).isEmpty
    ) yield  Mark(x, y, player) :: board
  }

  def computeAnyGameOld(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(Nil))
    case _ => for {
      game <- computeAnyGameOld(player.other, moves - 1)
      newMoveGame <- placeAnyMark(game.head, player)
    } yield newMoveGame :: game
  }


  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(Nil))
    case _ =>
      def won(board: Board): Boolean = {
        val size = 3
        def samePlayerMoves(moves: Traversable[Option[Player]]): Boolean = moves.head.isDefined &
          (moves.tail forall (_ == moves.head))

        def wonFirstDiagonal(): Boolean =
          samePlayerMoves( for(x <- 0 until size) yield find(board, x, x) )

        def wonSecondDiagonal(): Boolean =
          samePlayerMoves( for(x <- 0 until size) yield find(board, x, 2 - x) )

        def wonRow(): Boolean =
          (for(x <- 0 until size) yield samePlayerMoves(
            for(y <- 0 until size) yield find(board, x, y)
          )).toList contains true

        def wonCol(): Boolean =
          (for(y <- 0 until size) yield samePlayerMoves(
            for(x <- 0 until size) yield find(board, x, y)
          )).toList contains true

        wonFirstDiagonal | wonSecondDiagonal | wonRow | wonCol
      }

      for {
        game <- computeAnyGame(player.other, moves - 1)
        possibleGame <-
          if (won(game.head)) Traversable(game)
          else
           for (possibleMoveGame <- placeAnyMark(game.head, player)) yield possibleMoveGame :: game
      } yield possibleGame
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None


  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 6) foreach {g => printBoards(g); println()}
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!


}