package lab02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Step 2
 *
 * Now consider a RandomGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (RandomGenerator vs RangeGenerator), using patterns as needed
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private static final int START = 0;
    private static final int ONE = 1;
    private static final int ZERO = 0;
    private static final double HALF = 0.5;

    private int n;
    private int counter;

    public RandomGenerator(int n) {
        if (n <= ZERO) {
            throw new IllegalArgumentException();
        }
        this.n = n;
        this.reset();
    }

    @Override
    public Optional<Integer> next() {
        Optional<Integer> value;
        if (this.isOver()) {
            value = Optional.empty();
        } else {
            value = Optional.of(generateRandomBit());
        }
        this.incrementCounter();
        return value;
    }

    private void incrementCounter() {
        this.counter++;
    }

    static private int generateRandomBit() {
        return Math.random() > HALF ? ONE : ZERO;
    }

    @Override
    public void reset() {
        this.counter = START;
    }

    @Override
    public boolean isOver() {
        return this.counter >= this.n;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> list = new ArrayList<>();
        while (!this.isOver()) {
            list.add(this.next().get());
        }
        return Collections.unmodifiableList(list);
    }
}
