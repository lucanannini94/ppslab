package lab03

object Optionals extends App {

  sealed trait Option[A]

  object Option {
    case class Some[A](a: A) extends Option[A]
    case class None[A]() extends Option[A]

    /* curryed a due gruppi di argomenti per problemi di inferenza su A
       da un certo punto di vista è logico perché uno è un valore e l'altro è uno strategy
     */
    def pass[A](n: A) (predicate: A => Boolean): Option[A] = n match {
      case n if predicate(n) => Some(n)
      case _ => None()
    }
  }

  import Option._

  val o: Option[Int] = None()
  println(o)
  println(pass(-10) (_ >= 0))
}
