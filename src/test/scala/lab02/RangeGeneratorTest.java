package lab02;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class RangeGeneratorTest {

    private static final int START = 0;
    private static final int STOP = 10;
    private static final int TIMES = 5;

    private RangeGenerator rangeGenerator;

    @org.junit.Before
    public void setUp() throws Exception {
        this.rangeGenerator = new RangeGenerator(START, STOP);
    }

    @Test
    public void initiallyNotOver() {
        assertFalse(this.rangeGenerator.isOver());
    }

    @Test
    public void firstNextIsStart() {
        assertEquals(this.rangeGenerator.next(), Optional.of(START));
    }

    @Test
    public void multipleNextWork() {
        doSomeNext();
        assertEquals(this.rangeGenerator.next(), Optional.of(TIMES));
    }

    private void doSomeNext() {
        doMultipleNext(TIMES);
    }

    private void doMultipleNext(final int times) {
        for(int i = 0; i < times; i++) {
            this.rangeGenerator.next();
        }
    }

    @Test
    public void isOverWhenOutOfRange() {
        doNextUntilOutOfRange();
        assertTrue(this.rangeGenerator.isOver());
    }

    private void doNextUntilOutOfRange() {
        doMultipleNext(STOP - START + 1);
    }

    @Test
    public void givesEmptyIfOver() {
        doNextUntilOutOfRange();
        assertEquals(this.rangeGenerator.next(), Optional.empty());
    }

    @Test
    public void resetWorks() {
        doSomeNext();
        this.rangeGenerator.reset();
        assertEquals(this.rangeGenerator.next(), Optional.of(START));
    }

    @Test
    public void notOverWhileInRange() {
        doSomeNext();
        assertFalse(this.rangeGenerator.isOver());
    }

    @Test
    public void initiallyAllRemaining() {
        assertEquals(this.rangeGenerator.allRemaining(), createListInRange(START, STOP));
    }

    private List<Integer> createListInRange(final int start, final int stop) {
        return IntStream.rangeClosed(start, stop).boxed().collect(Collectors.toList());
    }

    @Test
    public void isOverAfterAllRemaining() {
        this.rangeGenerator.allRemaining();
        assertTrue(this.rangeGenerator.isOver());
    }

    @Test
    public void lastOneRemaining() {
        doAllButLastNext();
        assertEquals(this.rangeGenerator.allRemaining(), Arrays.asList(STOP));
    }

    private void doAllButLastNext() {
        doMultipleNext(STOP);
    }

    @Test
    public void resetsAfterAllRemaining() {
        this.rangeGenerator.allRemaining();
        this.rangeGenerator.reset();
        assertEquals(this.rangeGenerator.next(), Optional.of(START));
        assertFalse(this.rangeGenerator.isOver());
    }

    @Test
    public void allRemainingWorks() {
        doSomeNext();
        assertEquals(this.rangeGenerator.allRemaining(), createListInRange(TIMES, STOP));
    }

}