package lab02;

import org.junit.Test;

import static org.junit.Assert.*;

public class RangeGeneratorTestDegenerateCases {

    private static final int START = 0;

    private RangeGenerator rangeGenerator;

    @Test
    public void startAndStopAreEqual() {
        this.rangeGenerator = new RangeGenerator(START, START);
        this.rangeGenerator.next();
        assertTrue(this.rangeGenerator.isOver());
    }

    @Test (expected = IllegalArgumentException.class)
    public void stopIsLowerThanStart() {
        this.rangeGenerator = new RangeGenerator(START, START - 1);
    }

}