package lab02;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static org.junit.Assert.*;

public class RandomGeneratorTest {

    private static final int NUMBER = 11;
    private static final int ZERO = 0;
    private static final int ONE = 1;

    private Random random;
    private RandomGenerator randomGenerator;

    @Before
    public void setUp() throws Exception {
        random = new Random();
        randomGenerator = new RandomGenerator(NUMBER);
    }

    @Test
    public void returnsNNumbers() {
        List<Optional<Integer>> list = new ArrayList<>();
        while (!randomGenerator.isOver()) {
            list.add(randomGenerator.next());
        }
        assertEquals(list.size(), NUMBER);
    }

    @Test
    public void resetWorks() {
        doSomeNext(random.nextInt(NUMBER));
        randomGenerator.reset();
        assertFalse(randomGenerator.isOver());
    }

    private void doSomeNext(final int n) {
        for (int i = 0; i < n; i++)
            randomGenerator.next();
    }

    @Test
    public void isOverWorks() {
        doSomeNext(NUMBER);
        assertTrue(randomGenerator.isOver());
    }

    @Test (expected = IllegalArgumentException.class)
    public void illegalArguments() {
        randomGenerator = new RandomGenerator(-NUMBER);
    }

    @Test
    public void allRemainingCorrectSize() {
        int n = random.nextInt(NUMBER);
        doSomeNext(n);
        assertEquals(NUMBER - n, randomGenerator.allRemaining().size());
    }

    @Test
    public void generatesBits() {
        List<Integer> list = randomGenerator.allRemaining();
        for (int value : list) {
            assertTrue(isBit(value));
        }
    }

    private boolean isBit(final int value) {
        return value == ZERO || value == ONE;
    }

    @After
    public void tearDown() throws Exception {
    }
}